package whatcountry

import (
	"io/ioutil"

	"github.com/paulmach/orb"
	"github.com/paulmach/orb/geojson"
	"github.com/paulmach/orb/planar"
)

type Countries struct {
	fc *geojson.FeatureCollection
}

func LoadCountries(file string) (Countries, error) {
	b, err := ioutil.ReadFile(file)
	if err != nil {
		return Countries{}, err
	}
	featureCollection, err := geojson.UnmarshalFeatureCollection(b)
	if err != nil {
		return Countries{}, err
	}
	return Countries{fc: featureCollection}, nil
}

func (c *Countries) FindPoint(lon, lat float64) []string {
	countries := c.findCountries(orb.Point{lon, lat})
	var tn []string
	for _, t := range countries {
		tn = append(tn, t.Properties["iso_a2"].(string))
	}
	return tn
}

func (c *Countries) findCountries(point orb.Point) []geojson.Feature {
	countries := []geojson.Feature{}
	for _, feature := range c.fc.Features {
		// Try on a MultiPolygon to begin
		multiPoly, isMulti := feature.Geometry.(orb.MultiPolygon)
		if isMulti {
			if planar.MultiPolygonContains(multiPoly, point) {
				countries = append(countries, *feature)
			}
		} else {
			// Fallback to Polygon
			polygon, isPoly := feature.Geometry.(orb.Polygon)
			if isPoly {
				if planar.PolygonContains(polygon, point) {
					countries = append(countries, *feature)
				}
			}
		}
	}
	return countries
}
